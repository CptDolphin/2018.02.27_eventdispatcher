package AndroidX.ED.Event;

import AndroidX.ED.Dispatcher;
import AndroidX.ED.Listeners.ICallEnded;

import java.util.List;

public class CallEndedEvent implements IEvent {
    private int call_id;

    public CallEndedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallEnded> lista = Dispatcher.getInstance.getAllObjectsImplementingInterface(ICallEnded.class);
        for (ICallEnded listener : lista) {
            listener.callEnded(call_id);
        }
    }
}
