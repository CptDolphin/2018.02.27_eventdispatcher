package AndroidX.ED.Event;

import AndroidX.ED.Dispatcher;
import AndroidX.ED.Listeners.ICallStarted;

import java.util.List;

public class CallStartedEvent implements IEvent {
    private int call_id;

    public CallStartedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallStarted> lista = Dispatcher.getInstance.getAllObjectsImplementingInterface(ICallStarted.class);
        for (ICallStarted listener : lista) {
            listener.callStarted(call_id);
        }
    }
}
