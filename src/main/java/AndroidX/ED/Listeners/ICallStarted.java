package AndroidX.ED.Listeners;

public interface ICallStarted {
    void callStarted(int call_id);
    void outgoingCallStarted(int call_id);
}
