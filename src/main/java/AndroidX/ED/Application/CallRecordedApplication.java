package AndroidX.ED.Application;

import AndroidX.ED.Dispatcher;
import AndroidX.ED.Listeners.ICallEnded;
import AndroidX.ED.Listeners.ICallStarted;
import AndroidX.ED.SmsMessage;

public class CallRecordedApplication implements ICallStarted, ICallEnded {

    public CallRecordedApplication() {
        Dispatcher.getInstance.registerObject(this);
    }

    @Override
    public void callEnded(int call_id) {
        System.out.println("CallRecorded call ended " + call_id);
    }

    @Override
    public void callStarted(int call_id) {
        System.out.println("CallRecorded call started " + call_id);
    }

    @Override
    public void outgoingCallStarted(int call_id) {
        System.out.println("CallRecorded out call started " + call_id);

    }

}