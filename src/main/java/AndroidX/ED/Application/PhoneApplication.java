package AndroidX.ED.Application;

import AndroidX.ED.CallEntry;
import AndroidX.ED.CallType;
import AndroidX.ED.Dispatcher;
import AndroidX.ED.Listeners.ICallEnded;
import AndroidX.ED.Listeners.ICallStarted;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class PhoneApplication implements ICallStarted, ICallEnded {

    private List<CallEntry> rejestr = new LinkedList<>();

    public PhoneApplication() {
        Dispatcher.getInstance.registerObject(this);

    }

    @Override
    public void callEnded(int callId) {
        System.out.println("PHONE: call_end" + callId);
        for (CallEntry entry : rejestr) {
            if (entry.getCall_id() == callId) {
                entry.setTime_ended(LocalDateTime.now());
            }
        }
    }

    public boolean isAnyCallOngoing() {
        return rejestr.stream()
                .filter(entry -> entry.getTime_ended() == null).count() > 0;
    }

    @Override
    public void callStarted(int callId) {
        rejestr.add(new CallEntry(callId, LocalDateTime.now())); // Incoming
        System.out.println("PHONE: call_start" + callId);
    }

    @Override
    public void outgoingCallStarted(int callId) {
        rejestr.add(new CallEntry(callId, LocalDateTime.now(), CallType.OUTGOING));
        System.out.println("PHONE: out_call_start" + callId);
    }
}