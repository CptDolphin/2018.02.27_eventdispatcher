package AndroidX.ED.Application;

import AndroidX.ED.Dispatcher;
import AndroidX.ED.Listeners.ISMSListener;
import AndroidX.ED.SmsMessage;

import java.util.ArrayList;
import java.util.List;

public class SMSAplication implements ISMSListener {

    private List<SmsMessage> list = new ArrayList<>();

    public SMSAplication() {
        Dispatcher.getInstance.registerObject(this);
    }

    @Override
    public void smsReceived(SmsMessage message) {
        list.add(message);
        System.out.println("Message Received: " + message);
    }

    @Override
    public void smsSent(SmsMessage message) {
        list.add(message);
        System.out.println("Message sent: " + message);
    }

}
