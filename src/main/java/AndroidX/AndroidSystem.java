package AndroidX;

import AndroidX.ED.Application.CallRecordedApplication;
import AndroidX.ED.Application.PhoneApplication;
import AndroidX.ED.Application.SMSAplication;
import AndroidX.ED.Dispatcher;
import AndroidX.ED.Event.CallEndedEvent;
import AndroidX.ED.Event.CallStartedEvent;
import AndroidX.ED.Event.OutgoingCallEvent;
import AndroidX.ED.Listeners.ICallEnded;
import AndroidX.ED.Listeners.ICallStarted;

import java.util.ArrayList;
import java.util.List;

public class AndroidSystem implements ICallStarted,ICallEnded {
    private List<Notification> listaNotyfikacji;
    private List<Integer> ongoingCalls;

    public AndroidSystem() {
        Dispatcher.getInstance.registerObject(this);

        this.listaNotyfikacji = new ArrayList<>();
        this.ongoingCalls = new ArrayList<>();

        new CallRecordedApplication();
        new PhoneApplication();
        new SMSAplication();
    }

    @Override
    public void callEnded(int callId) {
        ongoingCalls.remove((Integer) callId);
        System.out.println("SYS: call_end" + callId);
    }

    @Override
    public void callStarted(int callId) {
        ongoingCalls.add(callId);
        System.out.println("SYS: call_start" + callId);
    }

    @Override
    public void outgoingCallStarted(int callId) {
        if (ongoingCalls.size() < 3) {
            ongoingCalls.add(callId);
            System.out.println("SYS: out_call_start" + callId);
        }
    }

    public void startCall(int id) {
        if (ongoingCalls.size() < 3) {
            Dispatcher.getInstance.dispatch(new CallStartedEvent(id));
        } else {
            System.out.println("Too many calls.");
        }
    }

    public void endCall(int id) {
        if (ongoingCalls.contains(id)) {
            System.out.println("Koncze polaczenie o id: " + id);
            Dispatcher.getInstance.dispatch(new CallEndedEvent(id));
        } else {
            System.out.println("Nie mogę zakończyć połączenia które nie zostało rozpoczęte.");
        }
    }

    public void outStartCall(int call_id) {
        if (ongoingCalls.size() < 3) {
            Dispatcher.getInstance.dispatch(new OutgoingCallEvent(call_id));
        }
    }}