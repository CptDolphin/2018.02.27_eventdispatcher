package AndroidX;

import AndroidX.ED.Dispatcher;
import AndroidX.ED.Event.SMSReceivedEvent;
import AndroidX.ED.SmsMessage;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        AndroidSystem manager = new AndroidSystem();

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String command = line.split(" ")[0];
            int call_id = Integer.parseInt(line.split(" ")[1]);
            if (command.equalsIgnoreCase("start")) {
//                EventDispatcher.instance.dispatch(new CallStartedEvent(call_id));
                manager.startCall(call_id);
            } else if (command.equalsIgnoreCase("stop")) {
                //                EventDispatcher.instance.dispatch(new CallStartedEvent(call_id));
                manager.endCall(call_id);
            }else if (command.equalsIgnoreCase("startout")) {
                //                EventDispatcher.instance.dispatch(new CallStartedEvent(call_id));
                manager.outStartCall(call_id);
            }else if(command.equalsIgnoreCase("sendsms")){
                String restOfCommand = line.split(" ", 3)[2];
                Dispatcher.getInstance.dispatch(new SMSReceivedEvent(new SmsMessage(restOfCommand)));
            }
        }
    }
}
